#!/usr/bin/env python

import pickle
import osmnx as ox
from os import path
import matplotlib.pyplot as plt


gmx = pickle.load(open('data/greater_mexico_city_all.pickle', 'rb'))
gsp = pickle.load(open('data/greater_sao_paulo_all.pickle', 'rb'))


fig = plt.figure(figsize=(5, 5), dpi=100)


degree_sequence=sorted(dict(gsp.degree()).values(), reverse=True)
plt.loglog(degree_sequence,
           marker='x',
           linewidth=0.3,
           color='black',
           alpha=0.5)

plt.xlim([1,10**6])
plt.ylim([1,10**1.2])



degree_sequence=sorted(dict(gmx.degree()).values(), reverse=True)
plt.loglog(degree_sequence,
           marker='+',
           linewidth=0.3,
           color='dimgrey',
           alpha=0.3)

plt.xlim([1,10**6])
plt.ylim([1,10**1.2])

ax = fig.get_axes()[0]
ax.set_ylabel("nodos")
ax.set_xlabel("grado")
fig.tight_layout()
fig.savefig('images/k_log_log_greaters.png')
