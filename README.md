# Análisis estructural de redes de calles: comparativo entre áreas metropolitanas de São Paulo y el Valle de México

Toda estructura es una manifestación de procesos subyacentes. Una red
de calles es una estructura generada por procesos subyacentes de
movilidad. Personas moviéndose y moviendo cosas conectan el
territorio.

Las ciudades son ecosistemas complejos. Ofrecen a sus habitantes
oportunidades de comercio, educación, servicios de salud, seguridad y
vida social. El soporte de estas actividades y sus beneficios
asociados requiere de enormes flujos de recursos.

Reorganizar las estructuras que vinculan aspectos biofísicos con
bienestar social y los buenos vivires es un reto prioritario de la
planificación urbana en todo el mundo.

La disponibilidad de datos y bibliotecas de software hacen posible la
creación de modelos computacionales sobre los cuáles hacer búsquedas
exhaustivas, refinamientos a los hallazgos, y despliegue de datos en
visualizaciones y otros instrumentos de análisis.

Este estudio consiste de un análisis exploratorio de las redes de
calles de las dos regiones metropolitanas más grandes de América
Mestiza.

## Suplemento digital

Se han generado visualizaciones de las redes de calles para cada
subregión política: en la zona urbana del Valle de México se trata de
41 municipios y 16 alcaldías; en el área metropolitana de São Paulo
son cinco ciudades y 16 *prefeituras*.

Se dibujan las redes de calles ubicando nodos y vínculos según sus
localizaciones geográficas. Se grafican diagramas de dispersión
log-log de los grados de conectividad de los nodos de cada red. Se
crean diagramas de coordenadas paralelas para contrastar las métricas
de todas las redes en conjunto. Finalmente se construyen histogramas
radiales de las orientaciones de las calles en cada territorio.

[Consulte el suplemento digital](https://rgarcia-herrera.gitlab.io/cdmx-saopaulo)

## Orientaciones de calles en el Valle de México

![histogramas radiales de alcaldías y municipios en el Valle de México](images/radial/mexico_alcaldias_municipios.png)

## Orientaciones de calles en la Macrometrópolis de São Paulo
![histogramas radiales de prefeituras y ciudades aledañas a São Paulo](images/radial/saopaulo_prefeituras.png)

## Distribución de grado

![Distribuciones de grado en ambos bbox](images/k_log_log_greaters.png)

Distribuciones de grados en redes empíricas construidas a partir de
bounding boxes. Los puntos marcados con el símbolo «+» corresponden al
bounding box de la Ciudad de México y su área conurbada. Los puntos
marcados con el símbolo «x» corresonden al bounding box de São Paulo y
su área conurbada.


## Topologías

La línea punteada corresponde al bounding box sobre el Valle de México. La línea sólida corresponde a la red de calles del bounding box sobre el área metropolitana de São Paulo.

![coordenadas paralelas con métricas de ambas redes](images/pc_bboxen_greater-sao-paulo-all.png)
