#!/usr/bin/env python

import argparse
from os import path
import pickle
from LatLon23 import LatLon, Latitude, Longitude

parser = argparse.ArgumentParser(description='Calculate area of pickled network.')
parser.add_argument('infile', type=argparse.FileType('rb'))
args = parser.parse_args()

g = pickle.load(args.infile)


X = [g.nodes[n]['x'] for n in g.nodes]
Y = [g.nodes[n]['y'] for n in g.nodes]


height = LatLon(Latitude(max(Y)),
                Longitude(min(X))).distance(LatLon(min(Y), min(X))) * 1000

width = LatLon(min(Y), min(X)).distance(LatLon(min(Y), max(X))) * 1000

print('"%s";%s;%s' % (g.name, args.infile.name, width*height))
