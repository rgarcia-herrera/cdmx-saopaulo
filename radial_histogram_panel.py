import svgwrite
from data import networks
from os import path
from slugify import slugify



def svg_panel(places, zone, filename, network_type='walk', cols=6):

    w = 1350
    h = 1000
    plot_offset = 200
    name_offset = 250
    
    dwg = svgwrite.Drawing(filename=filename)
    dwg.viewbox(width=(cols+1)*w, height=(len(places)/cols)*h)
    col = 0
    row = 0
    for place in places:
        place_name = place['place_name']
        slug = slugify("%s, %s" % (place_name, network_type))

        href = path.join('images',
                         "radial_hist_%s.png" % slug)

        dwg.add(svgwrite.image.Image(href,
                                     insert=(col * w, (row * h) + plot_offset)))

        short_name = place_name.split(',')[0]
        dwg.add(svgwrite.text.Text(short_name,
                                   insert=((col * w) + (w/2), (row * h) + name_offset),
                                   style="font-size: 100; text-anchor: middle;"))

        if col == cols:
            col = 0
            row += 1
        else:
            col += 1

    dwg.save()


svg_panel(networks.alcaldias_municipios, 'mexico', 'images/radial/mexico_alcaldias_municipios.svg', cols=5)
svg_panel(networks.saopaulo_prefeituras, 'brazil', 'images/radial/saopaulo_prefeituras.svg', cols=5)
