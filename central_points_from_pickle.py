#!/usr/bin/env python

import argparse
from os import path
import pickle

from sh import center_zoom

parser = argparse.ArgumentParser(description='Calculate central point.')
parser.add_argument('infile', type=argparse.FileType('rb'))
args = parser.parse_args()

g = pickle.load(args.infile)


X = [g.nodes[n]['x'] for n in g.nodes]
Y = [g.nodes[n]['y'] for n in g.nodes]


maxY = max(Y)
minY = min(Y)

maxX = max(X)
minX = min(X)

print(args.infile.name, center_zoom(minX, minY, maxX, maxY))
