from os import path
from data import networks
from slugify import slugify
import pickle

import pandas as pd
import matplotlib.pyplot as plt
from pandas.plotting import parallel_coordinates


def load_stats(places, network_types=networks.network_types):
    data = dict()
    for place in places:
        place_name = place['place_name']
        for network_type in network_types:
            slug = slugify("%s, %s" % (place_name, network_type))

            basic = path.join(networks.data_dir, 'stats',
                              "basic_stats_%s.pickle" % slug)

            extended = path.join(networks.data_dir, 'stats',
                                 "extended_stats_%s.pickle" % slug)

            if path.isfile(basic) and path.isfile(extended):
                with open(basic, 'rb') as fb, open(extended, 'rb') as fe:
                    bstats = pickle.load(fb)

                    bstats = {key.replace('_', ' '): bstats[key]
                              for key in bstats
                              if bstats[key] is not None}

                    streets_per_node_counts = bstats.pop('streets_per_node_counts'.replace('_', ' '))
                    streets_per_node_proportion = bstats.pop('streets_per_node_proportion'.replace('_', ' '))

                    extended_vars = [
                        'clustering_coefficient_avg',
                        'degree_centrality_avg',
                        'pagerank_max',
                        'pagerank_min',
                    ]

                    estats = pickle.load(fe)
                    estats = {key.replace('_', ' '): estats[key]
                              for key in estats
                              if key in extended_vars}

                    bstats.update(estats)

                    bstats = {key.replace('k avg', 'k promedio'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('intersection count', 'total de intersecciones'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('streets per node avg', 'promedio de calles por nodo'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('edge length total', 'longitud total de vínculos'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('edge length avg', 'longitud promedio de vínculos'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('street length total', 'longitud total de calles'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('street length avg', 'longitud promedio de calles'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('street segments count', 'total de segmentos de calle'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('node density km', 'densidad de nodos por km'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('intersection density km', 'densidad de intersecciones por km'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('edge density km', 'densidad de vínculos por km'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('street density km', 'densidad de calles por km'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('degree centrality avg', 'centralidad promedio'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('clustering coefficient avg', 'coeficiente de clustering promedio'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('pagerank max', 'pagerank máximo'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('pagerank min', 'pagerank mínimo'): bstats[key]
                              for key in bstats}

                    bstats.pop('circuity avg')
                    bstats.pop('self loop proportion')
                    data[slug] = bstats
            else:
                print('canna find %s or %s' % (basic, extended))
    return data




def plot_pc(data, places, prefix="pc", network_types=networks.network_types):

    df = pd.DataFrame.from_dict(data, orient='index')
    normalized_df=(df-df.min())/(df.max()-df.min())

    for place in places:
        place_name = place['place_name']
        for network_type in network_types:
            slug = slugify("%s, %s" % (place_name, network_type))
            if slug not in data:
                print("%s not in data" % slug)
                continue


            index1 = list(normalized_df.index)
            normalized_df['index1'] = index1

            fig, ax = plt.subplots()
            # background
            color = ['lightgrey'
                     for k in normalized_df.index]
            parallel_coordinates(normalized_df, 'index1', linewidth=0.7,
                                 color = color,
                                 ax=ax)


            # highlighted bboxen
            cdmx = pd.DataFrame.from_dict(
                {'greater-mexico-city-all': dict(normalized_df.loc['greater-mexico-city-all'])},
                orient='index')
            cdmx['index1'] = list(cdmx.index)

            parallel_coordinates(cdmx, 'index1',
                             color='#414141', linestyle='--', linewidth=0.8,
                                 ax=ax)


            saopaulo = pd.DataFrame.from_dict(
                {'greater-sao-paulo-all': dict(normalized_df.loc['greater-sao-paulo-all'])},
                orient='index')
            saopaulo['index1'] = list(saopaulo.index)

            parallel_coordinates(saopaulo, 'index1',
                                 color='black', linestyle='-', linewidth=0.7,
                                 ax=ax)

            legend = ax.legend()
            legend.remove()

            plt.xticks(rotation=90)

            fig.tight_layout()

            fig.savefig('images/%s_%s.png' % (prefix, slug),
                        dpi=200)

            plt.close()

print('=======> plotting bboxen')
plot_pc(load_stats(networks.bboxen + networks.todas_as_prefeituras, network_types=['all', 'walk']),
        places=networks.bboxen,
        prefix='pc_bboxen',
        network_types=['all', ])
