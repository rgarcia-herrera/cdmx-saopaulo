import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

sample_size = 5000
import csv

cdmx = {}
with open('data/greater_mexico_city_elevs.csv', 'r') as f:
    reader = csv.reader(f)
    for row in reader:
        cdmx[(row[0], row[1])] = int(row[2])


cdmx = np.random.choice(list(cdmx.values()), sample_size)

sp = {}
with open('data/greater_sao_paulo_elevs.csv', 'r') as f:
    reader = csv.reader(f)
    for row in reader:
        e = int(row[2])
        if e > 600:
            sp[(row[0], row[1])] = e

        
saopaulo = np.random.choice(list(sp.values()), sample_size)

sns.set(style="ticks")

df=pd.DataFrame.from_dict({'cdmx': cdmx, 'sp': saopaulo})

fig, ax = plt.subplots(figsize=(11, 11))


df_norm = (df - df.mean()) / (df.max() - df.min())

#df_norm = (df-df.mean())/df.std()
 
#sns.boxplot(data=df,
#            whis="range", palette="vlag", ax=ax)

sns.distplot(df_norm['cdmx'], kde = False, color='dimgrey')
sns.distplot(df_norm['sp'], kde = False, color='forestgreen')

# # sns.swarmplot(data=df,
# #               size=2, color=".3", linewidth=0, ax=ax)

ax.xaxis.grid(True)
ax.set(ylabel="frecuencia")
ax.set(xlabel="elevación normalizada")
sns.despine(trim=True, left=True)
fig.savefig("images/elev_boxplot.png")
