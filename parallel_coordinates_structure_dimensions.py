from os import path
from data import networks
from slugify import slugify
import pickle

import pandas as pd
import matplotlib.pyplot as plt
from pandas.plotting import parallel_coordinates


def load_stats(places, network_types=networks.network_types):
    data = dict()
    for place in places:
        place_name = place['place_name']
        for network_type in network_types:
            slug = slugify("%s, %s" % (place_name, network_type))

            basic = path.join(networks.data_dir, 'stats',
                              "basic_stats_%s.pickle" % slug)

            extended = path.join(networks.data_dir, 'stats',
                                 "extended_stats_%s.pickle" % slug)

            if path.isfile(basic) and path.isfile(extended):
                with open(basic, 'rb') as fb, open(extended, 'rb') as fe:
                    bstats = pickle.load(fb)

                    bstats = {key.replace('_', ' '): bstats[key]
                              for key in bstats
                              if bstats[key] is not None}

                    streets_per_node_counts = bstats.pop('streets_per_node_counts'.replace('_', ' '))
                    streets_per_node_proportion = bstats.pop('streets_per_node_proportion'.replace('_', ' '))

                    extended_vars = [
                        'clustering_coefficient_avg',
                        'betweenness_centrality_avg',
                        'degree_centrality_avg',
                        'pagerank_max',
                        'pagerank_min',
                        'diameter',
                        'radius',
                    ]

                    estats = pickle.load(fe)
                    estats = {key.replace('_', ' '): estats[key]
                              for key in estats
                              if key in extended_vars}

                    bstats.update(estats)

                    bstats = {key.replace('k avg', 'k promedio'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('intersection count', 'total de intersecciones'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('streets per node avg', 'promedio de calles por nodo'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('edge length total', 'longitud total de vínculos'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('edge length avg', 'longitud promedio de vínculos'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('street length total', 'longitud total de calles'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('street length avg', 'longitud promedio de calles'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('street segments count', 'total de segmentos de calle'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('node density km', 'densidad de nodos por km'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('intersection density km', 'densidad de intersecciones por km'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('edge density km', 'densidad de vínculos por km'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('street density km', 'densidad de calles por km'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('degree centrality avg', 'centralidad promedio'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('clustering coefficient avg', 'coeficiente de clustering promedio'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('pagerank max', 'pagerank máximo'): bstats[key]
                              for key in bstats}
                    bstats = {key.replace('pagerank min', 'pagerank mínimo'): bstats[key]
                              for key in bstats}
                    
                    data[slug] = bstats
            else:
                print('canna find %s or %s' % (basic, extended))
    return data



def plot_pc(data, places, prefix="pc", network_types=networks.network_types):

    df = pd.DataFrame.from_dict(data, orient='index')
    normalized_df=(df-df.min())/(df.max()-df.min())

    for place in places:
        place_name = place['place_name']
        for network_type in network_types:
            slug = slugify("%s, %s" % (place_name, network_type))
            if slug not in data:
                print("%s not in data" % slug)
                continue


            index1 = list(normalized_df.index)
            normalized_df['index1'] = index1

            fig, ax = plt.subplots()
            # background
            color = ['lightgrey'
                     for k in normalized_df.index]
            parallel_coordinates(normalized_df, 'index1',
                                 color = color,
                                 ax=ax)

            # highlighted in black
            current = pd.DataFrame.from_dict(
                {slug: dict(normalized_df.loc[slug])},
                orient='index')
            current['index1'] = list(current.index)

            parallel_coordinates(current, 'index1',
                                 color='black',
                                 ax=ax)

            legend = ax.legend()
            legend.remove()

            plt.xticks(rotation=90)

            fig.tight_layout()

            fig.savefig('images/pc/%s_%s.png' % (prefix, slug),
                        dpi=200)

            plt.close()





print('=======> plotting saopaulo_prefeituras')
plot_pc(load_stats(networks.saopaulo_prefeituras),
        places=networks.saopaulo_prefeituras,
        prefix='pc_saopaulo_prefeituras')

print('=======> plotting alcaldias_municipios')
plot_pc(load_stats(networks.alcaldias_municipios),
        places=networks.alcaldias_municipios,
        prefix='pc_alcaldias_municipios')

print('=======> plotting todas_as_prefeituras')
plot_pc(load_stats(networks.todas_as_prefeituras),
        places=networks.todas_as_prefeituras,
        prefix='pc_todas_as_prefeituras')


