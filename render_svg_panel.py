from os import path
from data import networks
from slugify import slugify
from jinja2 import Template
from yattag import Doc

def label_nt(network_type):
    if network_type == 'walk':
        nt='Red peatonal'
    elif network_type == 'bike':
        nt='Red ciclista'
    elif network_type == 'drive':
        nt='Red para vehículos motorizados'
    return nt

dirname = 'images'

with open('panel_h.svg', 'r') as f:
    template_h = Template(f.read())


data = dict()
for place in networks.saopaulo_prefeituras + networks.alcaldias_municipios:
    place_name = place['place_name']

    local = None
    if place_name in [p['place_name'] for p in networks.saopaulo_prefeituras]:
        local = 'saopaulo_prefeituras'
    elif place_name in [p['place_name'] for p in networks.alcaldias_municipios]:
        local = 'alcaldias_municipios'
    
    for network_type in networks.network_types:
        slug = slugify("%s, %s" % (place_name, network_type))

        with open(path.join(dirname,
                            "panel_%s.svg" % slug), 'w') as f:
            f.write(template_h.render(slug=slug,
                                      network_type=label_nt(network_type),
                                      local=local,
                                      place_name=place_name.split(',')[0]))
            



doc, tag, text = Doc().tagtext()
doc.asis('<!DOCTYPE html>')
with tag('html'):
    with tag('body'):
        with tag('title'):
            text('Análisis estructural de redes de calles: comparativo entre áreas metropolitanas de São Paulo y el Valle de México')
    with tag('body'):

        with tag('style'):
            text("""table, th, td {
                    border-bottom: 1px solid dimgrey;
                    border-collapse: collapse;
                    padding: 0.5em;
                    }
                 """)
        with tag('h1'):
            text('Análisis estructural de redes de calles: comparativo entre áreas metropolitanas de São Paulo y el Valle de México')

        text('Estos materiales suplementarios consisten de visualizaciones de datos analizados para cada sub-región de ambas metrópolis. Cada panel incluye dos gráficas de coordenadas paralelas, etiquetadas respectivamente "contexto local" y "contexto global". Contexto se refiere a las líneas grises sobre las cuáles se destaca la región bajo análisis, cada una representa las métricas de otras regiones. Local significa subregiones de la misma área conurbada, ya sea el Valle de México o el área metropolitana de São Paulo. Global se refiere a la colección completa de todas las subregiones. De esta manera ambas gráficas permiten comparar las métricas de una subregión cualquiera con sus vecinas y con sus equivalentes en el otro hemisferio.')
        
        with tag('p'):
            text('Consulte el repositorio para tener acceso a datos y software en torno a estas visualizaciones. ')
            with tag('a', href='https://gitlab.com/rgarcia-herrera/cdmx-saopaulo'):
                text('Repositorio en GitLab.')

        with tag('h2'):
            text('Alcaldías y Municipios de la zona urbana del Valle de México')
            
        with tag('table'):
            for place in networks.alcaldias_municipios:
                place_name = place['place_name']
            
                with tag('tr'):
                    with tag('td'):
                        text(place_name.split(',')[0])
                    for network_type in networks.network_types:
                        slug = slugify("%s, %s" % (place_name, network_type))                
                        with tag('td'):
                            with tag('a', href="panel_%s.svg" % slug):
                                text(network_type)


        with tag('h2'):
            text('Ciudades y Prefeituras en el área metropolitana de São Paulo')
            
        with tag('table'):
            for place in networks.saopaulo_prefeituras:
                place_name = place['place_name']
            
                with tag('tr'):
                    with tag('td'):
                        text(place_name.split(',')[0])
                    for network_type in networks.network_types:
                        slug = slugify("%s, %s" % (place_name, network_type))                
                        with tag('td'):
                            with tag('a', href="panel_%s.svg" % slug):
                                text(network_type)

with open(path.join(dirname,
                    "index.html"), 'w') as f:
    f.write(doc.getvalue())

    
