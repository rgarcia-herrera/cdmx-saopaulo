#!/usr/bin/env python

import argparse
from mapbox import Tilequery
import numpy as np
import csv

parser = argparse.ArgumentParser(description='get elevations for bboxen')
parser.add_argument('token')
args = parser.parse_args()

tilequery = Tilequery(access_token=args.token)



def get_elevs(north, south, west, east, filename):
    with open(filename, 'w') as f:
        writer = csv.writer(f)
    
        for y in np.linspace(north, south, 100):
            for x in np.linspace(west, east, 100):
                response = tilequery.tilequery("mapbox.mapbox-terrain-v2",
                                               lon=x, lat=y,
                                               radius=500, layers=['contour', ])
                g = response.geojson()
                for f in g.get('features', []):
                    writer.writerow(f['geometry']['coordinates'] + [f['properties']['ele'], ])



# get_elevs(north=20.0,
#           south=19.1,
#           west=-99.4,
#           east=-98.8,
#           filename='data/greater_mexico_city_elevs.csv')


get_elevs(north=-23.1,
          south=-24.0,
          west=-46.9,
          east=-46.3,
          filename='data/greater_sao_paulo_elevs.csv')
