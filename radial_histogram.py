#!/usr/bin/env python

import argparse
import pickle
import osmnx as ox
from os import path
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


parser = argparse.ArgumentParser(description='Radial histogram of street orientations.')
parser.add_argument('infile', type=argparse.FileType('rb'))
args = parser.parse_args()

radial_hist_name = 'radial_hist_' + path.basename(args.infile.name).replace('.pickle', '.png')
dirname = 'images'

if path.isfile(path.join(dirname, radial_hist_name)):
    print('will not overwrite found file %s' % radial_hist_name)
    exit()


g = pickle.load(args.infile)

ox.config(log_console=True, use_cache=True)


def reverse_bearing(x):
    return x + 180 if x < 180 else x - 180


# calculate edge bearings
Gu = ox.add_edge_bearings(ox.get_undirected(g))


city_bearings = []
for u, v, k, d in Gu.edges(keys=True, data=True):
    city_bearings.extend([d['bearing']] * int(d['length']))
    b = pd.Series(city_bearings)
    bearings = pd.concat([b, b.map(reverse_bearing)]).reset_index(drop='True')



def count_and_merge(n, bearings):
    # make twice as many bins as desired, then merge them in pairs
    # prevents bin-edge effects around common values like 0° and 90°
    n = n * 2
    bins = np.arange(n + 1) * 360 / n
    count, _ = np.histogram(bearings, bins=bins)

    # move the last bin to the front, so eg 0.01° and 359.99° will be binned together
    count = np.roll(count, 1)
    return count[::2] + count[1::2]




# function to draw a polar histogram for a set of edge bearings
def polar_plot(ax, bearings, n=9, title=''):

        bins = np.arange(n + 1) * 360 / n
        count = count_and_merge(n, bearings)
        _, division = np.histogram(bearings, bins=bins)
        frequency = count / float(count.sum())
        division = division[0:-1]
        width =  2 * np.pi / n

        ax.set_theta_zero_location('N')
        ax.set_theta_direction('clockwise')

        x = division * np.pi / 180
        bars = ax.bar(x, height=frequency, width=width, align='center', bottom=0, zorder=2,
                      color='dimgray', edgecolor='k', linewidth=0.5, alpha=0.5)

        ax.set_ylim(top=frequency.max())

        title_font = {'family':'FreeSans', 'size':14, 'weight':'bold'}
        xtick_font = {'family':'FreeSans', 'size':10, 'weight':'bold', 'alpha':1.0, 'zorder':3}
        ytick_font = {'family':'FreeSans', 'size': 9, 'weight':'bold', 'alpha':0.2, 'zorder':3}

        ax.set_title(title.upper(), y=1.05, fontdict=title_font)

        ax.set_yticks(np.linspace(0, max(ax.get_ylim()), 5))
        yticklabels = ['{:.2f}'.format(y) for y in ax.get_yticks()]
        yticklabels[0] = ''
        ax.set_yticklabels(labels=yticklabels, fontdict=ytick_font)

        xticklabels = ['N', '', 'E', '', 'S', '', 'W', '']
        ax.set_xticklabels(labels=xticklabels, fontdict=xtick_font)
        ax.tick_params(axis='x', which='major', pad=-2)



fig, ax = plt.subplots(1, 1, subplot_kw={'projection':'polar'})
polar_plot(ax, bearings.dropna(), n=36)
fig.savefig(path.join(dirname, radial_hist_name),
            dpi=200)
